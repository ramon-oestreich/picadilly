#instruções

### 

```sh
$ git clone https://bitbucket.org/ramon-oestreich/picadilly/src/master/
$ cd picadilly
$ cp .env.example .env
$ composer install
$ php artisan key:generate
$ configure .env com banco de dados
$ php artisan migrate
$ subir servidor  php artisan serve
```

## Exemplo .ENV

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=teste
DB_USERNAME=user
DB_PASSWORD=password

```

## Routes Users

| Route                  | Handler                    | Middleware | Name             | Domain |
| ---------------------- | -------------------------- | ---------- | ---------------- | ------ |
| GET /users           | UserController.index   | web       | users.index   |        |
| POST /users          | UserController.store   | web       | users.store   |        |
| PUT,PATCH /users/{user} | UserController.update  | web       | users.update  |        |
| DELETE /users/{user}    | UserController.destroy | web       | users.destroy |        |
