<?php


namespace App\Services;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserService
{

    /**
     * @var UserRepository
     */
    private $repository;
    /**
     * @var UserValidator
     */
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository =  $repository;
        $this->validator =  $validator;
    }

    public function create(array $data)
    {

        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }

        $data['password'] = bcrypt($data['password']);
        return $this->repository->create($data);
    }

    public function update(array $data, $id)
    {


        try{
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }catch (ValidatorException $e){
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }

        return $this->repository->skipPresenter()->update($data, $id);
    }
}
