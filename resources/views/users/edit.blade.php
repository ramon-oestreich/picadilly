@extends('layouts.master')

@section('content')
    <form method="post" action="{{ route('users.update', $user->id) }}">
        @method('PATCH')
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nome</label>
            <input type="text" class="form-control"  name="name" value={{$user->name}}>
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control"  name="email" value={{$user->email}}>
        </div>
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </form>
@endsection
