@extends('layouts.master')

@section('content')


    <table class="table table-hover table-bordered" style="margin-top: 10px">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Email</th>
            <th scope="col">Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <th scope="row">{{$user->id}}</th>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>


                <div class="btn-group">
                    <a href='{{url("users/$user->id/edit")}}'>
                    <button class="btn btn-primary" >
                       Editar
                    </button>
                    </a>
                </div>
                    <div class="btn-group" >
                        <form action="{{ route('users.destroy', $user->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">Excluir</button>
                        </form>

                    </div>
                <div class="btn-group">
                    <a href="/users/create">
                        <button class="btn btn-primary" >
                            Criar
                        </button>
                    </a>
                </div>

            </td>

        </tr>
        @endforeach

        </tbody>
    </table>
@endsection
